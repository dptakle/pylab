#!/bin/bash
set -e 

cd $(dirname ${0})/.. # I need to know where to start
. ci-cd/config.sh

if [ -z "${module_name}" ]; then
    echo "version not found"
    exit 1
else
    version=$(python -c "import ${module_name}; print(${module_name}.__version__)")
    if [ -z "${version}" ]; then
        echo "Module version not found"
        exit 1
    fi
fi

if [ -z "${artifactory_scheme_host}" ]; then
    echo "Artifactory host not found"
    exit 1
fi

if [ -z "${jfrog_api_key}" ]; then
    echo "JFrog API key not found"
    exit 1
fi

if [ -z "${package_type}" ]; then
    package_type=whl
fi

echo "Package type: ${package_type}"
echo " Artifactory: ${artifactory_scheme_host}"
echo "     Version: ${version}"
echo "      Module: ${module_name}"
echo "        Path: $PATH"
echo "         PWD: $(pwd)"
echo "   JFrog key: $(echo ${jfrog_api_key} | cut -b1-8)..."

jfrog rt c \
        --interactive false \
        --url ${artifactory_scheme_host}/artifactory \
        --apikey ${jfrog_api_key}

jfrog rt u \
    --build-name "${module_name}-${version}" \
    --build-number ${version} \
    "dist/${module_name}-${version}-*.${package_type}" \
    pypi-local
