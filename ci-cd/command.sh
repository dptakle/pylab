#!/bin/bash

#
# Hold this stuff for later
#
usage="usage: ${0} <build|test|clean|upload|bash>"
project_root=/project/ci-cd
image_name=dptakle/opencv_builder
hostname=mantis

if [ -z "${1}" ]; then
    echo "${usage}"
    exit 1
else
    command=${1}
fi

#
# Start the container with the provided command. Note: if the environment
# variables passed aren't set there is no foul.
#
# Args: ${1} - the command to run in the container
# Args: ${2} - interactive; true | false
#
# Returns: the exit status of the container
#
function call_docker {
    if [ "${2}" == "true" ]; then
        interactive=-it
    else
        interactive=
    fi

    docker pull ${image_name}
    docker run --rm \
        -e artifactory_scheme_host \
        -e jfrog_api_key \
        -v $(pwd):/project \
        -h ${hostname} \
        ${interactive} ${image_name} \
        ${1}
    return ${?}
}

cd $(dirname ${0})/.. # I need to know where to start

if [ "${command}" == "build" ]; then
    call_docker ${project_root}/build.sh false
elif [ "${command}" == "test" ]; then
    call_docker ${project_root}/test.sh false
elif [ "${command}" == "clean" ]; then
    call_docker ${project_root}/clean.sh false
elif [ "${command}" == "upload" ]; then
    call_docker ${project_root}/upload.sh false
elif [ "${command}" == "bash" ]; then
    call_docker /bin/bash true
else
    echo "${usage}"
    exit 1
fi

exit ${?}

