#!/bin/bash

#
# Cleanup the virtual environment we just created 
#
function cleanup {
    if [ ! -z "${VIRTUAL_ENV}" ]; then
        echo "Smashing virtual environment in ${VIRTUAL_ENV}"
        smash_target=${VIRTUAL_ENV}
        deactivate
        rm -rf ${smash_target}
    fi
}
trap cleanup EXIT

echo "Path: $PATH"
cd $(dirname ${0})/.. # I need to know where to start
echo "PWD: $(pwd)"

#
# Create and activate a virtual environment for this adventure
#
. /etc/profile
venv=/tmp/$(date | md5sum | cut -b1-8)
virtualenv ${venv}
. ${venv}/bin/activate

pytest
