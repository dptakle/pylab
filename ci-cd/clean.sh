#!/bin/bash

echo "Path: $PATH"
cd $(dirname ${0})/.. # I need to know where to start
echo "PWD: $(pwd)"

rm -rf dist
rm -rf build
rm -f *.egg
rm -f *.whl
rm -rf *.egg-info
